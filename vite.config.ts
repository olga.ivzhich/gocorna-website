export default {
    assetsInclude: [
        '**/*.svg',
        '**/*.png',
    ],
}
