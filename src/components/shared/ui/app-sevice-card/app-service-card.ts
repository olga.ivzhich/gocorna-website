import './app-service-card.css';

export type AppServiceCardProps = {
    image: HTMLImageElement;
    name: string;
    text: string
}

export default class AppServiceCard {
    private _parentNode: HTMLElement;
    private readonly cardImage: HTMLImageElement;
    private readonly cardName: string;
    private readonly cardText: string;

    constructor(parentNode: HTMLElement, props: AppServiceCardProps) {
        this._parentNode = parentNode;
        this.cardImage = props.image;
        this.cardText = props.text;
        this.cardName = props.name;
    }

     render() {
        const card = document.createElement("div");
        card.className = "app-service-card";
        card.appendChild(this.cardImage);

        const cardName = document.createElement("h3");
        cardName.innerText = this.cardName;
        cardName.className = "app-service-card__name";
        card.appendChild(cardName);

        const cardText = document.createElement("p");
        cardText.innerText = this.cardText;
        cardText.className = "app-service-card__text";
        card.appendChild(cardText);

        this._parentNode.appendChild(card);
    }
}
