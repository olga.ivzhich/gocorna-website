import './app-section-feature.css';
import AppButton from "../app-button/app-button";
import {AppHeading} from "../index";

export type AppSectionFeatureProps = {
    header: AppHeading;
    text: string;
    button: AppButton
}

export default class AppSectionFeature {
    private _parentNode: HTMLElement;
    private readonly header: AppHeading;
    private readonly sectionText: string;
    private readonly button: AppButton;

    constructor(parentNode: HTMLElement, props: AppSectionFeatureProps) {
        this._parentNode = parentNode;
        this.header = props.header;
        this.button = props.button;
        this.sectionText = props.text;
    }

    render() {
        const section = document.createElement("section");
        section.className = "app-section-feature";
        section.append(this.header);

        const sectionText = document.createElement("p");
        sectionText.innerText = this.sectionText;
        sectionText.className = "app-section-feature__text";
        section.append(sectionText);

        section.append(this.button);
        this._parentNode.appendChild(section);
    }
}
