import './app-counter.css';

export type AppCounterProps = {
    users: number;
    countries: number;
    medicalExperts: number;
}

export default class AppCounter extends HTMLElement {
    private readonly usersAmount: number = 0;
    private readonly countriesAmount: number = 0;
    private readonly medicalExpertsAmount: number = 0;

    constructor(props: AppCounterProps) {
        super();
        this.usersAmount = props.users ? props.users : 0;
        this.countriesAmount = props.countries ? props.users : 0;
        this.medicalExpertsAmount = props.medicalExperts ? props.medicalExperts : 0;
        this.createCounter();
    }

    private createCounter(): void {
        this.innerHTML = `
            <div class="app-counter">
                <div class="app-counter__item">
                    <p class="app-counter__item__number">${this.usersAmount} m</p>
                    <p class="app-counter__item__text">Users</p>
                </div>
                <div class="app-counter__item">
                    <p class="app-counter__item__number">${this.countriesAmount}</p>
                    <p class="app-counter__item__text">countries</p>
                </div>
                <div class="app-counter__item">
                    <p class="app-counter__item__number">${this.medicalExpertsAmount} +</p>
                    <p class="app-counter__item__text">medical experts</p>
                </div>
            </div>
        `;
    }
}
