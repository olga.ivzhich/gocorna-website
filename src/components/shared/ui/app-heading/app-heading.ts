import './app-heading.css';

export enum HeadingColor {
    RED = '#EC5863',
    BLUE = '#4285F4',
}

export type AppHeadingProps = {
    color: HeadingColor;
    text: string;
    coloredText: string
}

export default class AppHeading extends HTMLElement {
    private readonly headingColor: HeadingColor;
    private readonly headingText: string;
    private readonly headingColoredText: string;

    constructor(props: AppHeadingProps) {
        super();
        this.headingColor = props.color ?? HeadingColor.BLUE;
        this.headingColoredText = props.coloredText;
        this.headingText = props.text;
        this.createHeading();
    }

    private createHeading(): void {
        const coloredTextElement = `<span style="color: ${this.headingColor};">${this.headingColoredText}</span>`;
        const headingText = this.headingText.replace(new RegExp(this.headingColoredText, 'g'), coloredTextElement);
        this.innerHTML = `
            <h1 class="app-heading">${headingText}</h1>
        `;
    }
}
