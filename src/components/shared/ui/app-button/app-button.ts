import './app-button.css';
import {AppButtonMode} from "../../../../types/AppButtonMode";

const PRIMARY_MODE = 'primary';
const APP_BUTTON = 'app-button';
const BUTTON_PRIMARY = 'app-button-primary';
const BUTTON_SECONDARY = 'app-button-secondary';

export type AppButtonProps = {
    mode: AppButtonMode;
    text: string;
}

export default class AppButton extends HTMLElement {
    private readonly buttonClass: string;
    private readonly buttonText: string;

    constructor(props: AppButtonProps) {
        super();
        this.buttonText = props.text;
        this.buttonClass = this.setButtonClass(props.mode);
        this.createButton();
    }

    private createButton(): void {
        this.innerHTML = `
            <button class="${this.buttonClass}">
                ${this.buttonText}
            </button>
        `;
    }

    private setButtonClass(mode: string): string {
        return mode === PRIMARY_MODE
            ? `${APP_BUTTON} ${BUTTON_PRIMARY}`
            : `${APP_BUTTON} ${BUTTON_SECONDARY}`;
    }
}
