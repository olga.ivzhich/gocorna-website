import './styles/styles.css';
import './styles/fonts.css';

import {AppButton, AppCounter, AppHeading, AppServiceCard} from "./components/shared/ui";
import {AppHeroSection, AppStaySafeSection, AppTalkToExpertsSection, AppHealthcareFingerprintsSection} from "./sections";

customElements.define('app-button', AppButton);
customElements.define('app-heading', AppHeading);
customElements.define('app-counter', AppCounter);
customElements.define('app-service-card', AppServiceCard);

document.addEventListener("DOMContentLoaded", () => {
    const appNode = document.getElementById("app");

    if (!appNode) {
        console.error("Forgot to render div#app!!!!");
        return;
    }

    const heroSection = new AppHeroSection(appNode);
    heroSection.render();

    const staySafeSection = new AppStaySafeSection(appNode);
    staySafeSection.render();

    const talkToExpertsSection = new AppTalkToExpertsSection(appNode);
    talkToExpertsSection.render();

    const healthcareFingerprintsSection = new AppHealthcareFingerprintsSection(appNode);
    healthcareFingerprintsSection.render();
});

