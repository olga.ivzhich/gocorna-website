import './app-hero-section.css';
import logoImgUrl from '../../assets/logo/logo.svg';
import heroImage from '../../assets/images/hero-image.svg';
import {AppHeadingProps, HeadingColor} from "../../components/shared/ui/app-heading/app-heading";
import {AppButton, AppHeading, AppSectionFeature} from "../../components/shared/ui";
import {AppButtonProps} from "../../components/shared/ui/app-button/app-button";
import {AppSectionFeatureProps} from "../../components/shared/ui/app-section-feature/app-section-feature";
import AppWatchVideo from "./app-watch-video";


export default class AppHeroSection {
    private _parentNode: HTMLElement;

    constructor(parentNode: HTMLElement) {
        this._parentNode = parentNode;
    }

    public render() {
        // Create the 'section' element
        const section: HTMLDivElement = document.createElement('section') as HTMLDivElement;
        section.className = "hero-section";

        // Create the first 'div' element
        const contentDiv: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        contentDiv.className = "hero-content";

        //create header
        const header: HTMLDivElement = document.createElement('header') as HTMLDivElement;
        header.className = "hero-header";

        const logo = document.createElement('img');
        logo.src = <string>logoImgUrl;
        logo.alt = 'Logo';
        header.appendChild(logo);

        //create navigation links
        const nav = document.createElement('nav');
        const navList = document.createElement('ul');
        navList.classList.add('hero-nav-list');

        const navItems = ["HOME", "FEATURES", "SUPPORT", "CONTACT US"];

        navItems.forEach(itemText => {
            const listItem = document.createElement('li');
            const navItem = document.createElement('a');
            navItem.href = '#';
            navItem.textContent = itemText;
            listItem.append(navItem);
            navList.append(listItem);
        });

        nav.append(navList);
        header.append(nav);
        contentDiv.appendChild(header);

        //create hero content
        const familyHealthProps: AppHeadingProps = {
            color: HeadingColor.BLUE,
            text: 'Take care of yours family’s health',
            coloredText: 'health'
        }
        const familyHealthHeading = new AppHeading(familyHealthProps);

        const getStartedButtonProps: AppButtonProps = {
            mode: 'primary',
            text: 'Get Started'
        }

        const getStartedButton = new AppButton(getStartedButtonProps);

        const heroSectionFeatureProps: AppSectionFeatureProps = {
            header: familyHealthHeading,
            text: "All in one destination for COVID-19 health queries. Consult 10,000+ health workers about your concerns.",
            button:getStartedButton
        }
        const heroSectionFeature = new AppSectionFeature(contentDiv, heroSectionFeatureProps);
        heroSectionFeature.render();

        //add watch video section
        const watchVideoSection = new AppWatchVideo(contentDiv);
        watchVideoSection.render();

        // Create left panel 'div' element
        const leftPanelDiv: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        leftPanelDiv.className = "hero-left-panel";

        const downloadButtonProps: AppButtonProps = {
            mode: 'secondary',
            text: 'Download'
        }
        const downloadButton = new AppButton(downloadButtonProps);
        leftPanelDiv.appendChild(downloadButton);

        const heroImageElement = document.createElement('img');
        heroImageElement.src = <string>heroImage;
        heroImageElement.alt = 'Doctor';
        heroImageElement.className = 'hero-image';
        leftPanelDiv.appendChild(heroImageElement);

        // Add divs to the section
        section.appendChild(contentDiv);
        section.appendChild(leftPanelDiv);

        this._parentNode.appendChild(section);
    }
}
