import './app-watch-video.css';
import watchImgUrl from "../../assets/icons/watch-btn.svg";

export default class AppWatchVideo {
    private _parentNode: HTMLElement;

    constructor(parentNode: HTMLElement) {
        this._parentNode = parentNode;
    }

    render() {
        const section = document.createElement("section");
        section.className = "hero-watching-video";

        const imageContainer = document.createElement("div");
        imageContainer.className = "hero-watching-video__image-container";

        const watchImg = document.createElement('img');
        watchImg.src = watchImgUrl;
        watchImg.alt = 'Watch video';
        imageContainer.appendChild(watchImg);

        const watchVideoContent = document.createElement("div");
        watchVideoContent.className = "hero-watching-video__content";

        const watchVideoHeading = document.createElement("p");
        watchVideoHeading.className = "hero-watching-video__heading";
        watchVideoHeading.innerText = "Stay safe with GoCorona";
        watchVideoContent.appendChild(watchVideoHeading);

        const watchVideoText = document.createElement("p");
        watchVideoText.className = "hero-watching-video__text";
        watchVideoText.innerText = "Watch video";
        watchVideoContent.appendChild(watchVideoText);

        section.append(imageContainer);
        section.append(watchVideoContent);
        this._parentNode.appendChild(section);
    }
}
