import './app-talk-to-experts-section.css';
import talkRectImg from '../../assets/images/talk-rectangles.png';
import talkToExpertsImg from '../../assets/images/talk-image.png';
import {AppHeadingProps, HeadingColor} from "../../components/shared/ui/app-heading/app-heading";
import {AppButton, AppCounter, AppHeading, AppSectionFeature} from "../../components/shared/ui";
import {AppButtonProps} from "../../components/shared/ui/app-button/app-button";
import {AppSectionFeatureProps} from "../../components/shared/ui/app-section-feature/app-section-feature";
import {AppCounterProps} from "../../components/shared/ui/app-counter/app-counter";


export default class AppTalkToExpertsSection {
    private _parentNode: HTMLElement;

    constructor(parentNode: HTMLElement) {
        this._parentNode = parentNode;
    }

    public render() {
        // Create the 'section' element
        const section: HTMLDivElement = document.createElement('section') as HTMLDivElement;
        section.className = "talk-to-experts-section";

        // Create header section
        const headerSection: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        headerSection.className = "talk-to-experts-section-header";

        //add images to the header section
        const backgroundImage = document.createElement('img');
        backgroundImage.src = <string>talkRectImg;
        backgroundImage.alt = 'Stay Safe';
        headerSection.appendChild(backgroundImage);

        //add counter section to the header
        const counterProps: AppCounterProps = {
            users: 2,
            countries: 78,
            medicalExperts: 10000
        }

        const counter = new AppCounter(counterProps);
        headerSection.appendChild(counter);

        section.appendChild(headerSection);

        //Create content section
        const contentSection: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        contentSection.className = "talk-to-experts-section-content";

        // Create left div
        const leftDiv: HTMLDivElement = document.createElement('div') as HTMLDivElement;

        //Add section content
        const talkToExpertsProps: AppHeadingProps = {
            color: HeadingColor.BLUE,
            text: 'Talk to experts.',
            coloredText: 'experts.'
        }
        const talkToExpertsHeading = new AppHeading(talkToExpertsProps);

        const featureButtonProps: AppButtonProps = {
            mode: 'primary',
            text: 'Features'
        }
        const featureButton = new AppButton(featureButtonProps);

        const talkToExpertsFeatureProps: AppSectionFeatureProps = {
            header: talkToExpertsHeading,
            text: "Book appointments or submit queries into thousands of forums concerning health issues and prevention against noval Corona Virus.",
            button:featureButton
        }
        const talkToExpertsFeature = new AppSectionFeature(leftDiv, talkToExpertsFeatureProps);
        talkToExpertsFeature.render();

        contentSection.appendChild(leftDiv);

        // Create right div element
        const rightDiv: HTMLDivElement = document.createElement('div') as HTMLDivElement;

        //add images to the right section
        const talkToExpertImage = document.createElement('img');
        talkToExpertImage.src = <string>talkToExpertsImg;
        talkToExpertImage.alt = 'Stay Safe';
        rightDiv.appendChild(talkToExpertImage);

        contentSection.appendChild(rightDiv);

        // Add divs to the section
        section.appendChild(contentSection);

        this._parentNode.appendChild(section);
    }
}
