import './app-healthcare-fingerprints-section.css';
import talkRectImg from '../../assets/images/talk-rectangles.png';
import googlePlayImg from '../../assets/images/google-banner.png';
import appStoreImg from '../../assets/images/appstore-banner.png';
import doctorImg from '../../assets/icons/doctor.svg';
import heartImg from '../../assets/icons/heart.svg';
import humanImg from '../../assets/icons/human.svg';
import {AppHeadingProps, HeadingColor} from "../../components/shared/ui/app-heading/app-heading";
import {AppHeading, AppServiceCard} from "../../components/shared/ui";
import {AppServiceCardProps} from "../../components/shared/ui/app-sevice-card/app-service-card";


export default class AppHealthcareFingerprintsSection {
    private _parentNode: HTMLElement;

    constructor(parentNode: HTMLElement) {
        this._parentNode = parentNode;
    }

    public render() {
        // Create the 'section' element
        const section: HTMLDivElement = document.createElement('section') as HTMLDivElement;
        section.className = "healthcare-fingerprints-section";

        // Create header section
        const headerSection: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        headerSection.className = "healthcare-fingerprints-section-header";

        //add 'Healthcare at your Fingertips.' to the header section
        const healthCareProps: AppHeadingProps = {
            color: HeadingColor.RED,
            text: 'Healthcare at your Fingertips.',
            coloredText: 'Healthcare'
        }
        const healthCareHeading = new AppHeading(healthCareProps);
        headerSection.appendChild(healthCareHeading);

        //add section text
        const sectionText = document.createElement('p');
        sectionText.innerText = 'Bringing premium healthcare features to your fingertips. User friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.';
        sectionText.className = 'app-section-feature__text';
        headerSection.appendChild(sectionText);

        //Create content section
        const contentSection: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        contentSection.className = "healthcare-fingerprints-section-content";

        //add image to the content section
        const contentImage = document.createElement('img');
        contentImage.src = <string>talkRectImg;
        contentImage.className = 'healthcare-fingerprints-section-content__image';
        contentSection.appendChild(contentImage);

        //add container for cards
        const cardsContainer = document.createElement('div');
        cardsContainer.className = 'healthcare-fingerprints-section-content__cards';
        contentSection.appendChild(cardsContainer);

        //add cards to the container
        const cards: AppServiceCardProps[] = [
            {
                image: this.createImageElement(humanImg),
                name: 'Symptom Checker',
                text: 'Check if you are infected by COVID-19 with our Symptom Checker'
            },
            {
                image: this.createImageElement(doctorImg),
                name: '24x7 Medical support',
                text: 'Consult with 10,000+ health workers about your concerns.'
            },
            {
                image: this.createImageElement(heartImg),
                name: 'Conditions',
                text: 'Bringing premium healthcare features to your fingertips.'
            },
        ];

        cards.forEach(card => {
            const cardElement = new AppServiceCard(cardsContainer, card);
            cardElement.render();
        })

        //create footer section
        const footerSection: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        footerSection.className = "healthcare-fingerprints-section-footer";

        //add images to the footer section
        const footerImagesContainer = document.createElement('div');
        footerImagesContainer.className = 'healthcare-fingerprints-section-footer__images';

        const googlePlayImage = document.createElement('img');
        googlePlayImage.src = <string>googlePlayImg;
        googlePlayImage.alt = 'Google Play';
        footerImagesContainer.appendChild(googlePlayImage);

        const appleStoreImage = document.createElement('img');
        appleStoreImage.src = <string>appStoreImg;
        appleStoreImage.alt = 'Google Play';
        footerImagesContainer.appendChild(appleStoreImage);

        footerSection.appendChild(footerImagesContainer);

        section.appendChild(headerSection);
        section.appendChild(contentSection);
        section.appendChild(footerSection);

        this._parentNode.appendChild(section);
    }

    private createImageElement(imageName: {}): HTMLImageElement {
        const imageElement = document.createElement('img');
        imageElement.src = <string>imageName;
        // Return the new img element
        return imageElement;
    }
}
