import './app-stay-safe-section.css';
import staySafeImage from '../../assets/images/stay-image-with-rect.png';
import {AppHeadingProps, HeadingColor} from "../../components/shared/ui/app-heading/app-heading";
import {AppButton, AppHeading, AppSectionFeature} from "../../components/shared/ui";
import {AppButtonProps} from "../../components/shared/ui/app-button/app-button";
import {AppSectionFeatureProps} from "../../components/shared/ui/app-section-feature/app-section-feature";


export default class AppStaySafeSection {
    private _parentNode: HTMLElement;

    constructor(parentNode: HTMLElement) {
        this._parentNode = parentNode;
    }

    public render() {
        // Create the 'section' element
        const section: HTMLDivElement = document.createElement('section') as HTMLDivElement;
        section.className = "stay-safe-section";

        // Create the first 'div' element
        const content1: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        content1.className = "stay-safe-section-content";

        // Create the second 'div' element
        const content2: HTMLDivElement = document.createElement('div') as HTMLDivElement;
        content2.className = "stay-safe-section-content";

        const backgroundImage = document.createElement('img');
        backgroundImage.src = <string>staySafeImage;
        backgroundImage.alt = 'Stay Safe';
        content1.appendChild(backgroundImage);

        //create stay safe content
        const staySafeProps: AppHeadingProps = {
            color: HeadingColor.RED,
            text: 'Stay safe with GoCorona.',
            coloredText: 'Corona.'
        }
        const staySafeHeading = new AppHeading(staySafeProps);

        const featureButtonProps: AppButtonProps = {
            mode: 'primary',
            text: 'Features'
        }

        const featureButton = new AppButton(featureButtonProps);

        const staySafeSectionFeatureProps: AppSectionFeatureProps = {
            header: staySafeHeading,
            text: "24x7 Support and user friendly mobile platform to bring healthcare to your fingertips. Signup and be a part of the new health culture.",
            button:featureButton
        }
        const staySafeSectionFeature = new AppSectionFeature(content2, staySafeSectionFeatureProps);
        staySafeSectionFeature.render();

        // Add divs to the section
        section.appendChild(content1);
        section.appendChild(content2);

        this._parentNode.appendChild(section);
    }
}
